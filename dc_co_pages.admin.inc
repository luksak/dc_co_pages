<?php
/**
 * @file
 * Admin functions for Drupal Commerce Checkout Pages module.
 */

/**
 * Page callback for pages management.
 */
function dc_co_pages_manage_callback() {
  $pages = commerce_checkout_pages();
  $output = drupal_get_form('dc_co_pages_manage_form', $pages);
  return $output;
}

/**
 * Form to manage checkout pages.
 */
function dc_co_pages_manage_form($form, $form_state, $pages) {
  $form['pages']['#tree'] = TRUE;
  foreach ($pages as $page) {
    $id = isset($page['id']) ? $page['id'] : $page['page_id'];
    $form['pages'][$id]['#page'] = $page;
    $form['pages'][$id]['weight'] = array(
      '#type' => 'weight',
      '#delta' => 10,
      '#default_value' => $page['weight'],
      '#attributes' => array('class' => array('dc-co-page-weight')),
      '#disabled' => in_array($id, array('checkout', 'complete')),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  
  if (db_table_exists('commerce_checkout_page')) {
    $form['migration'] = array(
      '#type' => 'fieldset',
      '#title' => t('Migrate to Drupal Commerce'),
      '#description' => t('All the settings will be copied to the Drupal Commerce Checkout Page table.') .
        '<br />' . t('Only the checkout pages that are not already set in Drupal Comnmerce will be migrated.'),
    );
    $form['migration']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Migrate!'),
      //'#executes_submit_callback' => FALSE,
      '#submit' => array('_dc_co_pages_migrate_to_drupal_commerce'),
    );
  }

  return $form;
}

/**
 * Theming function for dc_co_pages_manage_form().
 */
function theme_dc_co_pages_manage_form($variables) {
  $form = $variables['form'];

  $header = array(t('Name'), t('Weight'), t('Status Cart'), t('Buttons'), array('data' => t('Operations'), 'colspan' => 2));
  $rows   = array();
  foreach (element_children($form['pages'], TRUE) as $k) {
    $page = $form['pages'][$k]['#page'];

    $row = array();
    $row[] = t('!title (Machine name: !machine) !help', array(
      '!title'   => $page['title'],
      '!machine' => '<small>' . (isset($page['id']) ? $page['id'] : $page['page_id']) . '</small>',
      '!help'    => "<div class=\"description\">{$page['help']}</div>",
    ));
    $row[] = drupal_render($form['pages'][$k]['weight']);
    $row[] = $page['status_cart'] ? t('Yes') : t('No');
    $row[] = $page['buttons'] ? t('Yes') : t('No');
    $row[] = l(t('Edit'), "admin/commerce/config/checkout/custom-pages/{$k}/edit");

    if (!isset($page['locked'])) {
      $row[] = l(t('Delete'), "admin/commerce/config/checkout/custom-pages/{$k}/delete");
    }
    elseif (isset($page['changed'])) {
      $row[] = l(t('Reset'), "admin/commerce/config/checkout/custom-pages/{$k}/delete");
    }
    else {
      $row[] = t('Default');
    }

    # locked weight should not be draggable.
    $draggable = !empty($form['pages'][$k]['weight']['#disabled']) ? '' : 'draggable';
    $rows[] = array('data' => $row, 'class' => array($draggable));
  }

  drupal_add_tabledrag('dc-co-pages-manage-form-table', 'order', 'sibling', 'dc-co-page-weight');
  $output  = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'dc-co-pages-manage-form-table')));
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Submit handler for dc_co_pages_manage_form().
 */
function dc_co_pages_manage_form_submit($form, &$form_state) {
  $v = &$form_state['values'];
  foreach (array_keys($v['pages']) as $id) {
    $weight = $v['pages'][$id]['weight'];
    if ($weight != $form['pages'][$id]['#page']['weight']) {
      if (!$page = dc_co_pages_page_load($id)) {
        $page = array('id' => $id);
      }

      $page['weight'] = $weight;
      dc_co_pages_page_save($page);
    }
  }

  // @TODO: DOCME.
  if (module_exists('commerce_checkout_progress')) {
    $cache_id = 'commerce_checkout_progress:pages';
    cache_clear_all($cache_id, 'cache');
  }
}

/**
 * Form to edit checkout page.
 */
function dc_co_pages_edit_page_form($form, $form_state, $page = array()) {
  if (!isset($page['edit'])) {
    $page['edit'] = $page;
  }
  $form['title'] = array(
    '#type' => 'textfield',
    '#size' => 30,
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => isset($page['edit']['title']) ? $page['edit']['title'] : '',
  );

  $form['page_id'] = array(
    '#type' => 'machine_name',
    '#title' => t('Machine name'),
    '#maxlength' => 32,
    '#required' => TRUE,
    '#default_value' => isset($page['edit']['page_id']) ? $page['edit']['page_id'] : '',
    '#disabled' => isset($page['locked']),
    '#machine_name' => array(
      'exists' => 'dc_co_pages_page_load',
      'source' => array('title'),
    ),
  );

  $form['buttons_labels'] = array(
    '#type' => 'fieldset',
    '#title' => t('Buttons labels'),
  );

  $form['buttons_labels']['back_value'] = array(
    '#type' => 'textfield',
    '#size' => 30,
    '#title' => t('Back value'),
    //'#required' => TRUE,
    '#default_value' => isset($page['edit']['back_value']) ? $page['edit']['back_value'] : '',
  );
  
  $form['buttons_labels']['submit_value'] = array(
    '#type' => 'textfield',
    '#size' => 30,
    '#title' => t('Submit value'),
    //'#required' => TRUE,
    '#default_value' => isset($page['edit']['submit_value']) ? $page['edit']['submit_value'] : '',
  );

  $form['help'] = array(
    '#type' => 'textarea',
    '#title' => t('Help'),
    # '#required' => TRUE,
    '#description' => t('Help text displayed to end user on checkout page.'),
    '#default_value' => isset($page['edit']['help']) ? $page['edit']['help'] : '',
  );

  $form['status_cart'] = array(
    '#type' => 'checkbox',
    '#title' => t('Status cart'),
    '#default_value' => isset($page['status_cart']) ? $page['status_cart'] : FALSE,
    '#disabled' => isset($page['locked']),
  );

  $form['buttons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Buttons'),
    '#default_value' => isset($page['buttons']) ? $page['buttons'] : FALSE,
    '#disabled' => isset($page['locked']),
  );

  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  if (isset($page['page_id'])) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete page'),
    );
    if (isset($page['locked'])) {
      $form['actions']['delete']['#value'] = t('Reset to defaults');
    }
  }

  return $form;
}

/**
 *
 * @param type $form
 * @param type $form_state
 */
function dc_co_pages_edit_page_form_submit($form, $form_state) {
  if($form_state['clicked_button']['#id'] == 'edit-delete') {
    drupal_goto('admin/commerce/config/checkout/custom-pages/' . $form_state['values']['page_id'] . '/delete');
  }

  $v = $form_state['values'];

  $record['page_id']    = $v['page_id'];
  //@TODO #1321380 filter_xss_admin() allows many html tags to be used but commerce seems to filter them out on display.
  $record['title'] = filter_xss_admin($v['title']);
  $record['help']  = filter_xss_admin($v['help']);
  $record['back_value'] = filter_xss_admin($v['back_value']);
  $record['submit_value']  = filter_xss_admin($v['submit_value']);
  $record['status_cart'] = $v['status_cart'];
  $record['buttons'] = $v['buttons'];

  if (FALSE !== dc_co_pages_page_save($record)) {
    // @TODO: DOCME.
    if (module_exists('commerce_checkout_progress')) {
      $cache_id = 'commerce_checkout_progress:pages';
      cache_clear_all($cache_id, 'cache');
    }

    drupal_set_message(t('The page has been saved'));
    $form_state['redirect'] = 'admin/commerce/config/checkout/custom-pages';
  }
}

/**
 * Page delete form.
 */
function dc_co_pages_delete_page_form($form, $form_state, $page) {
  $form['#page'] = $page;
  $question = 'Are you sure you want to @action the page %title?';
  $action   = !empty($page['custom']) ? 'delete' : 'reset';
  $question = t($question, array('%title' => $page['title'], '@action' => $action));
  $path = 'admin/commerce/config/checkout/custom-pages';
  return confirm_form($form, $question, $path);
}

function dc_co_pages_delete_page_form_submit($form, &$form_state) {
  dc_co_pages_page_delete($form['#page']);
  $form_state['redirect'] = 'admin/commerce/config/checkout/custom-pages';

  // @TODO: DOCME.
  if (module_exists('commerce_checkout_progress')) {
    $cache_id = 'commerce_checkout_progress:pages';
    cache_clear_all($cache_id, 'cache');
  }
}

/**
 * Migrate to Drupal Commerce.
 */
function _dc_co_pages_migrate_to_drupal_commerce($form, &$form_state) {
  if (db_table_exists('commerce_checkout_page')) {
    db_query('INSERT IGNORE INTO {commerce_checkout_page} SELECT * FROM {dc_co_pages}');
    drupal_set_message(t('Settings copied to Drupal Commerce Checkout pages.'));
  }
}